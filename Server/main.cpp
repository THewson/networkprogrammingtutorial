#include <iostream>
#include <string>

#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>

#include "GameMessages.h"

const unsigned short PORT = 5456;
RakNet::RakPeerInterface* pPeerInterface = nullptr;
int nextClientID = 1;

void SendNewClientID(RakNet::RakPeerInterface* pPeerInterface, RakNet::SystemAddress& address);

int main()
{

	// Startup the server, and start it listening to clients
	std::cout << "Starting up the server..." << std::endl;

	// Initialise the Raknet peer Interface first
	pPeerInterface = RakNet::RakPeerInterface::GetInstance();

	// Create a socket descriptor to describe this connection
	RakNet::SocketDescriptor sd(PORT, 0);

	pPeerInterface->Startup(32, &sd, 1);
	pPeerInterface->SetMaximumIncomingConnections(32);
	
	RakNet::Packet* packet = nullptr;
	
	// Startup a thread to ping clients every second
	//std::thread pingThread(SendClientPing, pPeerInterface);

	while (true) {			
			for (packet = pPeerInterface->Receive(); packet; pPeerInterface->DeallocatePacket(packet), packet = pPeerInterface->Receive()) {
			switch (packet->data[0]) {
			case ID_NEW_INCOMING_CONNECTION: 
				std::cout << "A connection is incoming.\n";
				SendNewClientID(pPeerInterface, packet->systemAddress);
				break;
			case ID_DISCONNECTION_NOTIFICATION:
				std::cout << "A client has disconnected.\n";
				break;
			case ID_CONNECTION_LOST:
				std::cout << "A client lost the connection.\n";
				break;
			default:
				std::cout << "Received a message with an unknown id: " << packet->data[0];
				break;
			}
		}
	}
}

void SendNewClientID(RakNet::RakPeerInterface* pPeerInterface, RakNet::SystemAddress& address) {
	RakNet::BitStream bs;
	bs.Write((RakNet::MessageID)GameMessages::ID_SERVER_SET_CLIENT_ID);
	bs.Write(nextClientID);

	nextClientID++;

	pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, address, false);
}