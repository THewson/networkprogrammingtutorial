#pragma once

#include "Application.h"
#include <glm/mat4x4.hpp>

#include <RakPeerInterface.h>
#include <MessageIdentifiers.h> 
#include <BitStream.h>

#include "GameObject.h"

class Client : public aie::Application {
public:

	Client();
	virtual ~Client();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

	// Initialise the connection
	void HandleNetworkConnection();
	void InitaliseClientConnection();
	
	// Handle incoming packets
	void HandleNetworkMessages();

	// Handles Client ID being set
	void OnSetClientIDPacket(RakNet::Packet* packet);
	void SendClientGameObject();

protected:

	int m_ClientID;

	RakNet::RakPeerInterface* m_pPeerInterface;
	GameObject m_GameObject;

	const char* IP = "127.0.0.1";
	const unsigned short PORT = 5456;

	glm::mat4	m_viewMatrix;
	glm::mat4	m_projectionMatrix;
};