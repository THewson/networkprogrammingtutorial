#pragma once

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

struct GameObject {
	glm::vec3 position;
	glm::vec4 colour;
};